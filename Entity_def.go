package Fielddefs


// Entity Definition - Created By:  David Szkilnyk 04-06-2016


func Entity_Define(d *BOdef) {

     d.BOName = "Entity"
     d.DMName = "entity"
     d.Oid = 500

// Field List...
     d.AddField( 501, "Entity_id", "Entity_id", "PK", DTInt64, false, false, "", "", true )
     d.AddField( 502, "Code", "Code", "Code", DTString, false, false, "", "", true )
     d.AddField( 503, "Description", "Description", "Description", DTString, false, false, "", "", true )
     d.AddField( 504, "T_id", "T_id", "T", DTInt64, false, false, "", "", true )
     d.AddField( 505, "User_id", "User_id", "User", DTInt64, false, false, "", "", true )
     d.AddField( 507, "Rec_update", "Rec_update", "Rec_update", DTDateTime, false, false, "", "", true )
     d.AddField( 508, "Rec_options", "Rec_options", "Rec_options", DTInteger, false, false, "", "", true )

  //      rec_create time.Time / rec_update time.Time / rec_options  int / t_id int / p_id int
  //   d.BOExtra_Define(true,true,true,true,true)



     d.Fields["Entity_id"].Ispk = true
}

var Entity_map *BOdef

func Entity_def() *BOdef {
     if Entity_map == nil {
          Entity_map = new(BOdef)
          Entity_map.Fields = make(map[string]*Fielddefinition)
          Entity_Define(Entity_map)

          MapRepoInstance().Register("Entity", Entity_map)
     }
     return Entity_map
}
