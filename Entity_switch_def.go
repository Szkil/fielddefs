package Fielddefs


// Entity_switch Definition - Created By:  David Szkilnyk 04-06-2016


func Entity_switch_Define(d *BOdef) {

     d.BOName = "Entity_switch"
     d.DMName = "Entity_switch"
     d.Oid = 600

// Field List...
     d.AddField( 601, "Entity_switch_id", "Entity_switch_id", "Entity_switch", DTInt64, false, false, "", "", true )
     d.AddField( 602, "Entity_id", "Entity_id", "Entity", DTInt64, false, false, "", "", true )
     d.AddField( 603, "Description", "Description", "Description", DTString, false, false, "", "", true )
     d.AddField( 604, "Asinteger", "Asinteger", "Asinteger", DTInteger, false, false, "", "", true )
     d.AddField( 605, "Asstring", "Asstring", "Asstring", DTString, false, false, "", "", true )
     d.AddField( 606, "Internal_func", "Internal_func", "Internal_func", DTString, false, false, "", "", true )
     d.AddField( 607, "T_id", "T_id", "T", DTInt64, false, false, "", "", true )
     d.AddField( 608, "User_id", "User_id", "User", DTInt64, false, false, "", "", true )
     d.AddField( 610, "Rec_update", "Rec_update", "Rec_update", DTDateTime, false, false, "", "", true )
     d.AddField( 611, "Rec_options", "Rec_options", "Rec_options", DTInteger, false, false, "", "", true )
     d.AddField( 612, "Reserved", "Reserved", "Reserved", DTInteger, false, false, "", "", true )
     d.AddField( 613, "Rec_code", "Rec_code", "Rec_code", DTString, false, false, "", "", true )

  //      rec_create time.Time / rec_update time.Time / rec_options  int / t_id int / p_id int
  //   d.BOExtra_Define(true,true,true,true,true)

     d.AddField( -1, "oEntity", "", "Entity", DTObject, false, true, "", "Entity", true )

     d.Fields["Entity_switch_id"].Ispk = true
}

var Entity_switch_map *BOdef

func Entity_switch_def() *BOdef {
     if Entity_switch_map == nil {
          Entity_switch_map = new(BOdef)
          Entity_switch_map.Fields = make(map[string]*Fielddefinition)
          Entity_switch_Define(Entity_switch_map)

          MapRepoInstance().Register("Entity_switch", Entity_switch_map)
     }
     return Entity_switch_map
}
