package Fielddefs

// Repository - holds an Active instance Interface of a related BOManager

type MapRepo struct {
	oDictionary map[string]*BOdef
}

func NewMapRepo() *MapRepo {
	var result *MapRepo
	result = new(MapRepo)
	result.oDictionary = make(map[string]*BOdef)

	return result
}

var oMapRepo *MapRepo

func MapRepoInstance() *MapRepo {
	if oMapRepo == nil {
		oMapRepo = NewMapRepo()
	}
	return oMapRepo
}

// Register filter in the factory
func (f *MapRepo) Register(Name string, I *BOdef) {
	f.oDictionary[Name] = I
}

// Return a requested Filter interface   // IBOManager
func (f *MapRepo) Get(Name string) *BOdef {
	return f.oDictionary[Name]
}
