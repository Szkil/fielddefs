package Fielddefs

// LKBase Definition - Created By:  David Szkilnyk 04-05-2015

func LKBase_Define(d *BOdef) {

	d.BOName = "LKBase"
	d.DMName = "LKBase"
	d.Oid = 2900

	// Field List...
	d.AddField(2901, "Base_id", "Base_id", "PK", DTInt64, false, false, "", "", true)
	d.AddField(2902, "Code", "Code", "Code", DTString, false, false, "", "", true)
	d.AddField(2903, "Description", "Description", "Description", DTString, false, false, "", "", true)
	d.AddField(2904, "Reserved", "Reserved", "Reserved", DTInteger, false, false, "", "", true)
	d.AddField(2905, "T_id", "T_id", "T", DTInt64, false, false, "", "", true)
	d.AddField(2906, "User_id", "User_id", "User", DTInt64, false, false, "", "", true)
	d.AddField(2908, "Rec_update", "Rec_update", "Rec_update", DTDateTime, false, false, "", "", true)
	d.AddField(2909, "Rec_options", "Rec_options", "Rec_options", DTInteger, false, false, "", "", true)

	//      rec_create time.Time / rec_update time.Time / rec_options  int / t_id int / p_id int
	//   d.BOExtra_Define(true,true,true,true,true)

	d.Fields["Base_id"].Ispk = true
}

var LKBase_map *BOdef

func LKBase_def() *BOdef {
	if LKBase_map == nil {
		LKBase_map = new(BOdef)
		LKBase_map.Fields = make(map[string]*Fielddefinition)
		LKBase_Define(LKBase_map)

		MapRepoInstance().Register("LKBase", LKBase_map)
	}
	return LKBase_map
}

// Leave reasons
func Leave_reason_Define(d *BOdef) {

	d.BOName = "Leave_reason"
	d.DMName = "Leave_reasons"
	d.Oid = 3300

	// Field List...
	d.AddField(3301, "Leave_reason_id", "Leave_reason_id", "PK", DTInt64, false, false, "", "", true)
	d.AddField(3302, "Code", "Code", "Code", DTString, false, false, "", "", true)
	d.AddField(3303, "Description", "Description", "Description", DTString, false, false, "", "", true)
	d.AddField(3304, "Reserved", "Reserved", "Reserved", DTInteger, false, false, "", "", true)
	d.AddField(3305, "T_id", "T_id", "T", DTInt64, false, false, "", "", true)
	d.AddField(3306, "User_id", "User_id", "User", DTInt64, false, false, "", "", true)
	d.AddField(3308, "Rec_update", "Rec_update", "Rec_update", DTDateTime, false, false, "", "", true)
	d.AddField(3309, "Rec_options", "Rec_options", "Rec_options", DTInteger, false, false, "", "", true)

	//      rec_create time.Time / rec_update time.Time / rec_options  int / t_id int / p_id int
	//   d.BOExtra_Define(true,true,true,true,true)

	d.Fields["Leave_reason_id"].Ispk = true
}

var Leave_reason_map *BOdef

func Leave_reason_def() *BOdef {
	if Leave_reason_map == nil {
		Leave_reason_map = new(BOdef)
		Leave_reason_map.Fields = make(map[string]*Fielddefinition)
		Leave_reason_Define(Leave_reason_map)

		MapRepoInstance().Register("Leave_reason", Leave_reason_map)
	}
	return Leave_reason_map
}
