package Fielddefs

import (
	"testing"
	"fmt"
)

func TestIBOdef(t *testing.T) {
	fmt.Println("TestIBOdef..")

	var lodef IBOdef
	lodef = Ev_def()
	if (lodef.OID() == 0) {
		t.Error("Error ID is 0. ")    // some case this maybe true but in this case its not.
	}

	if lodef.Boname() == "" {
		t.Error("Error BOName is empty")
	}

	if lodef.Dmname() == "" {
		t.Error("Error DMName is empty")
	}

	lsField := "Ev_id"
	loFld := lodef.AField(lsField)
	if loFld == nil {
		t.Errorf("Error Field not found - %v ",lsField)
	}

	loFld = lodef.GetPK()
	if loFld == nil {
		t.Error("Error Primary Key Field not found ")
	}

	if loFld.BOFieldName() == "" {
		t.Error("Error Primary Key Fieldname is empty.")
	}




    // todo: Tests - verify a iterator is needed.




}
