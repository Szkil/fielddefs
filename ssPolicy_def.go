package Fielddefs

// ssPolicy Definition - Created By:  David Szkilnyk 14-11-2014


func ssPolicy_Define(d *BOdef) {
	d.BOName = "ssPolicy"
	d.DMName = "sspolicy"
	d.Oid = 1100

	// Field List...
	d.AddField(1101, "Sspolicy_id", "Sspolicy_id", "Sspolicy_id", DTInt64, false, false, "", "", false)
	d.AddField(1102, "Code", "Code", "Code", DTString, true, true, "", "", true)
	d.AddField(1103, "Description", "Description", "Description", DTString, true, true, "", "", true)
	d.AddField(1104, "Asinteger", "Asinteger", "AsInteger", DTInteger, false, false, "", "", true)
	d.AddField(1105, "Company_id", "Company_id", "Company", DTInt64, false, false, "", "", false)
	d.AddField(1106, "T_id", "T_id", "Tenant", DTInt64, false, false, "", "", false)
	d.AddField(1107, "User_id", "User_id", "User", DTInt64, false, false, "", "", false)
	d.AddField(1109, "Rec_update", "Rec_update", "updated", DTDateTime, false, false, "", "", false)
	d.AddField(1110, "Rec_options", "Rec_options", "Options", DTInteger, false, false, "", "", false)

	//      rec_create time.Time / rec_update time.Time / rec_options  int / t_id int / p_id int
	//   d.BOExtra_Define(true,true,true,true,true)

	d.Fields["Sspolicy_id"].Ispk = true
}

var ssPolicy_map *BOdef

func SSPolicy_def() *BOdef {
	if ssPolicy_map == nil {
		ssPolicy_map = new(BOdef)
		ssPolicy_map.Fields = make(map[string]*Fielddefinition)
		ssPolicy_Define(ssPolicy_map)

		MapRepoInstance().Register("ssPolicy", ssPolicy_map)
	}
	return ssPolicy_map
}
