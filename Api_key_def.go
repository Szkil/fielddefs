package Fielddefs

// Api_key Definition - Created By:  David Szkilnyk 27-04-2016

func Api_key_Define(d *BOdef) {

	d.BOName = "Api_key"
	d.DMName = "api_keys"
	d.Oid = 900

	// Field List...
	d.AddField(901, "Api_key_id", "Api_key_id", "api_key_id", DTInt64, false, false, "", "", false)
	d.AddField(902, "Code", "Code", "Code", DTString, false, false, "", "", true)
	d.AddField(903, "Description", "Description", "Description", DTString, false, false, "", "", true)
	d.AddField(904, "Person_id", "Person_id", "Person", DTInt64, false, false, "", "", true)
	d.AddField(905, "Options", "Options", "Options", DTInteger, false, false, "", "", true)
	d.AddField(906, "Definition", "Definition", "definition", DTString, false, false, "", "", true)
	d.AddField(907, "T_id", "T_id", "t_id", DTInt64, false, false, "", "", true)
	d.AddField(908, "User_id", "User_id", "User", DTInt64, false, false, "", "", true)
	d.AddField(909, "Rec_update", "Rec_update", "rec_update", DTDateTime, false, false, "", "", true)
	d.AddField(910, "Rec_options", "Rec_options", "rec_options", DTInteger, false, false, "", "", true)

	//      rec_create time.Time / rec_update time.Time / rec_options  int / t_id int / p_id int
	//   d.BOExtra_Define(true,true,true,true,true)

	d.Fields["Api_key_id"].Ispk = true
}

var Api_key_map *BOdef

func Api_key_def() *BOdef {
	if Api_key_map == nil {
		Api_key_map = new(BOdef)
		Api_key_map.Fields = make(map[string]*Fielddefinition)
		Api_key_Define(Api_key_map)

		MapRepoInstance().Register("Api_key", Api_key_map)
	}
	return Api_key_map
}
