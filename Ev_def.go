package Fielddefs

// Ev Definition - Created By:  David Szkilnyk 28-04-2016


func Ev_Define(d *BOdef) {

	d.BOName = "Ev"
	d.DMName = "ev"
	d.Oid = 50

	// Field List...
	d.AddField(51, "Ev_id", "Ev_id", "PK", DTInt64, false, false, "", "", true)
	d.AddField(52, "Name", "Name", "Name", DTString, false, false, "", "", true)
	d.AddField(53, "Asdata", "Asdata", "Data", DTString, false, false, "", "", true)
	d.AddField(54, "Asvalue", "Asvalue", "AsValue", DTFloat, false, false, " ", "", true)
	d.AddField(55, "T_id", "T_id", "T", DTInt64, false, false, "", "", true)
	d.AddField(56, "User_id", "User_id", "User", DTInt64, false, false, "", "", true)
	d.AddField(57, "Rec_update", "Rec_update", "Rec_update", DTDateTime, false, false, "", "", true)
	d.AddField(58, "Rec_options", "Rec_options", "Rec_options", DTInteger, false, false, "", "", true)

	//      rec_create time.Time / rec_update time.Time / rec_options  int / t_id int / p_id int
	//   d.BOExtra_Define(true,true,true,true,true)


	d.Fields["Ev_id"].Ispk = true
}

var Ev_map *BOdef

func Ev_def() *BOdef {
	if Ev_map == nil {
		Ev_map = new(BOdef)
		Ev_map.Fields = make(map[string]*Fielddefinition)
		Ev_Define(Ev_map)

		MapRepoInstance().Register("Ev", Ev_map)
	}
	return Ev_map
}
