package Fielddefs



// Ua Definition - Created By:  David Szkilnyk 03-07-2016

func Ua_Define(d *BOdef) {

     d.BOName = "Ua"
     d.DMName = "Ua"
     d.Oid = 1000

// Field List...
     d.AddField( 1001, "Ua_id", "Ua_id", "Ua", DTInt64, false, false, "", "", true )
     d.AddField( 1002, "Handle", "Handle", "Handle", DTString, false, false, "", "", true )
     d.AddField( 1003, "Kiosk_email", "Kiosk_email", "Kiosk_email", DTString, false, false, "", "", true )
     d.AddField( 1004, "Entity_id", "Entity_id", "Entity", DTInt64, false, false, "", "", true )
     d.AddField( 1005, "Person_id", "Person_id", "Person", DTInt64, false, false, "", "", true )
     d.AddField( 1006, "Tenant_id", "Tenant_id", "Tenant", DTInt64, false, false, "", "", true )
     d.AddField( 1007, "T_id", "T_id", "T", DTInt64, false, false, "", "", true )
     d.AddField( 1008, "User_id", "User_id", "User", DTInt64, false, false, "", "", true )
     d.AddField( 1010, "Rec_update", "Rec_update", "Rec_update", DTDateTime, false, false, "", "", true )
     d.AddField( 1011, "Rec_options", "Rec_options", "Rec_options", DTInteger, false, false, "", "", true )

  //      rec_create time.Time / rec_update time.Time / rec_options  int / t_id int / p_id int
  //   d.BOExtra_Define(true,true,true,true,true)

     d.Fields["Ua_id"].Ispk = true
}

var Ua_map *BOdef

func Ua_def() *BOdef {
     if Ua_map == nil {
          Ua_map = new(BOdef)
          Ua_map.Fields = make(map[string]*Fielddefinition)
          Ua_Define(Ua_map)

          MapRepoInstance().Register("Ua", Ua_map)
     }
     return Ua_map
}
