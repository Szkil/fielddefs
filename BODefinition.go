package Fielddefs

import "sort"
import "strings"

/*
    the aim is to keep everything as lean as possible
	- only include func's that we need him

// REVIEW: also consider removing the extra formats. if I need
 theres alot of crap here that I really wont use
*/
// transposed to reflect kind  - only problem we have is decimal/double
const (
	DTUnknown = iota
	DTString = iota
	DTSmallint = iota
	DTInteger = iota
	DTWord = iota
	DTBoolean = iota
	DTFloat = iota
	DTDecimal = iota
	DtDate = iota
	DTTime = iota
	DTDateTime = iota
	DTBytes = iota
	DTVarBytes = iota
	DTAutoInc = iota
	DTBlob = iota
	DTMemo = iota
	DTGraphic = iota
	DTFmtMemo = iota
	DTInt64 = iota
	DTTypedBinary = iota
	DTObject = iota
)

func IsDTaInteger(Datatype     int) bool {
	switch Datatype {
	case DTUnknown     : return false
	case DTString      : return false
	case DTSmallint    : return true
	case DTInteger     : return true
	case DTWord        : return true
	case DTBoolean     : return false
	case DTFloat       : return false
	case DTDecimal     : return false
	case DtDate        : return false
	case DTTime        : return false
	case DTDateTime    : return false
	case DTBytes       : return false
	case DTVarBytes    : return false
	case DTAutoInc     : return false
	case DTBlob        : return false
	case DTMemo        : return false
	case DTGraphic     : return false
	case DTFmtMemo     : return false
	case DTInt64       : return true
	case DTTypedBinary : return false
	case DTObject      : return false
	default : return false
	}
	return false
}

func ISDTaString(Datatype     int) bool {
	switch Datatype {
	case DTString      : return true
	// REVIEW: Add DTMemo or DBFmtMeo when I actually have a need for it - also consider removing the extra formats.
	default     : return false
	}
}

type BOdef struct {
	BOName string
	DMName string
	Oid    int

	Title string
	// expirement if I want as an interface or static struct
	Fields map[string]*Fielddefinition
}

type IBOdef interface {
	Boname() string
	Dmname() string
	OID() int    // OID value
	GetPK() IFieldDef

    DMFieldOrderbyId() string
	AddField(aid int, asFieldName string, asDMFieldName string,
	asDisplayName string, aiDatatype int, abNodeDisplay bool, abListDisplay bool, asMask string,
	asClassname string, abExportable bool)

	// I need the array interface to bleed threw
	// see if i can find the - a facade to handle it.
	AField(value string) IFieldDef
	Iterator(afn func(aItem *Fielddefinition) bool)

	//AsTitle(value interface{})
	AsTitle() string
	// request an iterator
}

// todo: review a number of these fields are just for internal use of the clas
// as such we really should on. surface the interfacing fields when needed
// there should be a getter as a
//    - BOFieldName()
// done in such a way the it is unique from the internal struct value - internal structure should be lowercase or
// if not needed to be surfaced if surfaced then capital is leading first up then thats it.

// alot of the fields are only set in the initial define so don't surface them here.
// if a conflict the use a adjective - Is / What / Get
type IFieldDef interface {
	BOFieldName() string
	DMFieldName() string
	DisplayName() string

	PictureMask() string

	IsNodedisplay() bool
	IsListdisplay() bool

	GetDatatype() int  // I need to establish what datatype enums I am able to support
	IsSupporting() string
	IsExportable() bool
	IsPK() bool
	IsSK() bool

	ID() int
	Options() int

	//Fieldstate    int
	IsNotFilterable() bool // default is false  - so most fields will be false
}

type Fielddefinition struct {
	BOFieldname   string
	DMFieldname   string
	Displayname   string

	Picture       string

	Nodedisplay   bool
	Listdisplay   bool

	Datatype      int  // I need to establish what datatype enums I am able to support
	Supporting    string
	Exportable    bool
	Ispk          bool

	Issk          bool
	Id            int
	options       int
	fieldstate    int

	notfilterable bool // default is false  - so most fields will be false
}

func Z_IBODef(i interface{}) (IBOdef, bool) {
	if iInt, ok := i.(IBOdef); ok {
		return iInt, true
	} else {
		return nil, false
	}
}

func ZA_IBODef(i interface{}) IBOdef {
	if iInt, ok := i.(IBOdef); ok {
		return iInt
	} else {
		return nil
	}
}



func (f *Fielddefinition) IsNotFilterable() bool {
	return f.notfilterable
}

//AddField( "Bank_id", "Bank_id", "Bank_ID", dsDatatype.dtInteger, false, false, "", "", false );
func (self *BOdef) AddField(aid int, asFieldName string, asDMFieldName string,
asDisplayName string, aiDatatype int, abNodeDisplay bool, abListDisplay bool, asMask string,
asClassname string, abExportable bool) {

	loFD := new(Fielddefinition)

	loFD.Id = aid
	loFD.Ispk = false
	loFD.BOFieldname = asFieldName
	loFD.DMFieldname = asDMFieldName
	loFD.Displayname = asDisplayName

	loFD.Datatype = aiDatatype
	loFD.Picture = asMask

	loFD.Nodedisplay = abNodeDisplay
	loFD.Listdisplay = abListDisplay

	loFD.Supporting = asClassname
	loFD.Exportable = abExportable

	//loFD.Datatype = fltInteger // int
	self.Fields[asFieldName] = loFD

}

func (self *BOdef) AddPKField(value string) {
	// Find the field and set it as PK = true
	self.Fields[value].Ispk = true

}

func (self *BOdef) DMFieldOrderbyId() string {
	var result string
	//todo: eval can this be made more efficently as we are creating a new map
	// the slice I understand but the map?
	// stack then pop
	loFields := make(map[int]*Fielddefinition)
	for _, loFD := range self.Fields {
		// exclude relations which are -1 and DMFieldname == ""
		if loFD.Id > 0 && loFD.DMFieldname != "" {
			loFields[loFD.Id] = loFD
		}
	}

	// keys need the DMFieldname
	var keys []int
	for _, k := range loFields {
		keys = append(keys, k.Id)
	}
	sort.Ints(keys)
	// problem is k is a int - the key to the map of self.fields is a the BOfieldName
	// I am creating a new lofields based on id(int)
	// what I need to do is create a simple slice. of the names I need.
	// the lofields is doing too much.

	for _, k := range keys {
		result = result + loFields[k].DMFieldname + ", "
	}
	result = strings.TrimSuffix(result, ", ")

	return result
}

// IsNotFilterable

//  rec_create time.Time / rec_update time.Time / rec_options  int / t_id int / p_id int
/*
func (self *BODefinition) BOExtra_Define(abRec_create bool, abRec_update bool,
	abRec_options bool, abt_id bool, abp_id bool) {

	if abRec_create == true {
		self.AddField("rec_create", "rec_create", "Create", dtDateTime, false, false, "", "", true)
	}
	if abRec_update == true {
		self.AddField("rec_update", "rec_update", "Update", dtDateTime, false, false, "", "", true)
	}
	if abRec_options == true {
		self.AddField("rec_options", "rec_options", "Options", dtInteger, false, false, "", "", true)
	}
	if abt_id == true {
		self.AddField("t_id", "t_id", "Tenant", dtInteger, false, false, "", "", true)
	}
	if abp_id == true {
		self.AddField("p_id", "p_id", "Person", dtInteger, false, false, "", "", true)
	}

}
*/

// Interface support IBODef

func (bo *BOdef) Boname() string {
	return bo.BOName
}

func (bo *BOdef) Dmname() string {
	return bo.DMName
}

func (bo *BOdef) AsTitle() string {
	return bo.Title
}


func (bo *BOdef) AField(value string) IFieldDef {
	return bo.Fields[value]
}

// return true from closure it will end
func (bo *BOdef) Iterator(afn func(aItem *Fielddefinition) bool) {
	for _, Item := range bo.Fields {
		if afn(Item) {
			break
		}
	}
}

func (bo *BOdef) OID() int {
	return bo.Oid
}

func (bo *BOdef) GetPK() IFieldDef {
	for _, aField := range bo.Fields {
		if aField.Ispk {
			return aField
		}
	}
	return nil
}


// Interface support IFieldef

func (fd *Fielddefinition)  BOFieldName() string {
	return fd.BOFieldname
}

func (fd *Fielddefinition)  DMFieldName() string {
	return fd.DMFieldname
}

func (fd *Fielddefinition) DisplayName() string {
	return fd.Displayname
}

func (fd *Fielddefinition) PictureMask() string {
	return fd.Picture
}

func (fd *Fielddefinition) IsNodedisplay() bool {
	return fd.Nodedisplay
}

func (fd *Fielddefinition) IsListdisplay() bool {
	return fd.Listdisplay
}

func (fd *Fielddefinition) GetDatatype() int {
	// I need to establish what datatype enums I am able to support
	return fd.Datatype
}

func (fd *Fielddefinition) IsSupporting() string {
	return fd.Supporting
}

func (fd *Fielddefinition) IsExportable() bool {
	return fd.Exportable
}

func (fd *Fielddefinition) IsPK() bool {
	return fd.Ispk
}

func (fd *Fielddefinition) IsSK() bool {
	return fd.Issk
}

func (fd *Fielddefinition) ID() int {
	return fd.Id
}

func (fd *Fielddefinition) Options() int {
	return fd.options
}



