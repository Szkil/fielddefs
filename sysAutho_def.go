package Fielddefs

// sysAutho Definition - Created By:  David Szkilnyk 14-11-2014

func sysAutho_Define(d *BOdef) {
	d.BOName = "sysAutho"
	d.DMName = "sysautho"
	d.Oid = 1200

	// Field List...
	d.AddField(1201, "Sysautho_id", "Sysautho_id", "sysAutho_id", DTInt64, false, false, "", "", false)
	d.AddField(1202, "Api_key_id", "Api_key_id", "Api_key_id", DTInt64, true, true, "", "", true)
	d.AddField(1203, "Entity_id", "Entity_id", "Entity_id", DTInt64, false, false, "", "", false)
	d.AddField(1204, "Person_id", "Person_id", "Person_id", DTInt64, false, false, "", "", false)
	d.AddField(1205, "Tenant_id", "Tenant_id", "Tenant", DTInt64, false, false, "", "", false)
	d.AddField(1206, "Laston", "Laston", "Last On", DTDateTime, false, false, "", "", true)
	d.AddField(1207, "T_id", "T_id", "Tenant", DTInt64, false, false, "", "", false)
	d.AddField(1208, "User_id", "User_id", "User", DTInt64, false, false, "", "", false)
	d.AddField(1209, "Rec_update", "Rec_update", "updated", DTDateTime, false, false, "", "", false)
	d.AddField(1210, "Rec_options", "Rec_options", "Options", DTInteger, false, false, "", "", false)

	//      rec_create time.Time / rec_update time.Time / rec_options  int / t_id int / p_id int
	//   d.BOExtra_Define(true,true,true,true,true)

	d.Fields["Sysautho_id"].Ispk = true
}

var SysAutho_map *BOdef

func SysAutho_def() *BOdef {
	if SysAutho_map == nil {
		SysAutho_map = new(BOdef)
		SysAutho_map.Fields = make(map[string]*Fielddefinition)
		sysAutho_Define(SysAutho_map)

		MapRepoInstance().Register("sysAutho", SysAutho_map)
	}
	return SysAutho_map
}
